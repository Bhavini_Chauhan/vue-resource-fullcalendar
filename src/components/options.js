// export default {
//   // All elemets with the class 'selectable' selectable.
//   selectables: [".selectable"]
// };

export default {
    // Class for the selection-area-element
    class: "selection-area",

    selectedClass: "selected",

    // px, how many pixels the point should move before starting the selection
    startThreshold: 10,

    // Disable the selection functionality for touch devices
    disableTouch: false,

    // Enable single-click selection
    singleClick: true,

    // Query selectors from elements from which the siblings can be selected
    // containers: [".icon-grid"],

    // Query selectors from elements which can be selected
    selectables: [".icon"],

    // Query selectors for elements from where a selection can be start
    startareas: ["html"],

    // Query selectors for elements which will be used as boundaries for the selection
    boundaries: ["html"],

    //------------------------------------------
    // SELECT
    //------------------------------------------

    onSelect(evt) {
        // Check if clicked element is already selected
        const selected = evt.target.classList.contains(this.options.selectedClass);

        // Remove class if user don't press the control key or ⌘ key and the
        // current target is already selected
        if (!evt.originalEvent.shiftKey && !evt.originalEvent.metaKey) {
            // Remove class from every element which is selected
            evt.selectedElements.forEach(s =>
                s.classList.remove(this.options.selectedClass)
            );

            // Clear previous selection
            this.clearSelection();
        }

        if (!selected) {
            // Select element
            evt.target.classList.add(this.options.selectedClass);
            this.keepSelection();
        } else {
            // Deselect element
            evt.target.classList.remove(this.options.selectedClass);
            this.removeFromSelection(evt.target);
        }
    },

    //------------------------------------------
    // START
    //------------------------------------------

    // this event is triggered on drag start
    onStart(evt) {
        // Get selected elements
        const selectedElements = evt.selectedElements;

        // Remove class if the user is'nt pressing the control key or ⌘ key
        if (!evt.originalEvent.shiftKey && !evt.originalEvent.metaKey) {
            // Deselect all elements
            selectedElements.forEach(s =>
                s.classList.remove(this.options.selectedClass)
            );
            // Clear previous selection
            this.clearSelection();
        }
    },

    //------------------------------------------
    // MOVE
    //------------------------------------------

    // this event is triggered on mouse drag, not move
    onMove(evt) {
        // Get elements added or removed
        const selectedElements = evt.selectedElements;
        const removedElements = evt.changedElements.removed;

        // Add a custom class to the elements which where selected.
        selectedElements.forEach(s => s.classList.add(this.options.selectedClass));

        // Remove the class from elements which where removed
        // since the last selection
        removedElements.forEach(s =>
            s.classList.remove(this.options.selectedClass)
        );
    },

    //------------------------------------------
    // STOP
    //------------------------------------------

    // this event is triggered on dragend
    onStop(evt) {
        this.keepSelection();
    }
};
