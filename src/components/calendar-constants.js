const YOUR_CONSTS = {

    APPOINTMENT: 1,
    APPT_BLOCK_TIME: 2,
    APPT_BACKGROUND_NOTE: 3,
    APPT_MAKE_AVAILABLE: 4,
    APPT_WEBSITE_UNAVAILABLE: 5,
    DELETE_APPOINTMENT: 6,
    FULLDAY_TIME: '00:00',
    DAY_VIEW: "resourceTimeGridDay",
    WEEK_VIEW: "resourceTimeGridWeek",
    MONTH_VIEW: "resourceDayGridMonth",
}

export default {
    install(Vue, options) {
        Vue.prototype.$getConst = (key) => {
            return YOUR_CONSTS[key]
        }
    }
}