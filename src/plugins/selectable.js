import Selection   from '@simonwep/selection-js';

/**
 * Is responsible for the selection area
 * @returns {*}
 */
export default new Selection({
    // Class for the selection-area-element
    class: "selection-area",

    selectedClass: "selected",

    // px, how many pixels the point should move before starting the selection
    startThreshold: 10,

    // Disable the selection functionality for touch devices
    disableTouch: false,

    // Enable single-click selection
    singleClick: true,

    // Query selectors from elements from which the siblings can be selected
    // containers: [".selectable-time-grid"],

    // Query selectors from elements which can be selected
    selectables: [".selectable-time"],

    // Query selectors for elements from where a selection can be start
    startareas: [".selectable-time-wrapper"],

    // Query selectors for elements which will be used as boundaries for the selection
    boundaries: [".selectable-time-wrapper"],
}).on('start', ({inst, selected, oe}) => {

    // Remove class if the user isn't pressing the control key or ⌘ key
    if (!oe.ctrlKey && !oe.metaKey) {

        // Unselect all elements
        for (const el of selected) {
            el.classList.remove('selected');
            inst.removeFromSelection(el);
        }

        // Clear previous selection
        inst.clearSelection();
    }

}).on('move', ({changed: {removed, added}}) => {

    // Add a custom class to the elements that where selected.
    for (const el of added) {
        el.classList.add('selected');
    }

    // Remove the class from elements that where removed
    // since the last selection
    for (const el of removed) {
        el.classList.remove('selected');
    }

}).on('stop', ({inst}) => {
    // Remember selection in case the user wants to add smth in the next one
    inst.keepSelection();
});