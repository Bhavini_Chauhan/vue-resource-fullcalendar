import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import constants from './components/calendar-constants'

Vue.config.productionTip = false
Vue.use(constants);
new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
